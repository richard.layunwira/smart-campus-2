package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

public class BusN1EstimateTime {

    @SerializedName("StopName")
    public StopName stop_name;

    @SerializedName("EstimateTime")
    public String estimate_time;

    @SerializedName("StopUID")
    public String stop_uid;

    public void setStop_Name(StopName stop_name) { this.stop_name = stop_name; }

    public void setEstimate_Time(String estimate_time) { this.estimate_time = estimate_time; }

    public void setStop_UID(String stop_uid) { this.stop_uid = stop_uid; }

}
