package com.example.richard.smartcampus;

public class bus_schedule_item {

    String header_1;
    String plate_number;
    String position_bus;
    String time_bus;

    public bus_schedule_item(String header_1, String plate_number, String position_bus, String time_bus)
    {
        this.header_1 = header_1;
        this.plate_number = plate_number;
        this.position_bus = position_bus;
        this.time_bus = time_bus;
    }

}
