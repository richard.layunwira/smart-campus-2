package com.example.richard.smartcampus;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterBusStopItem extends ArrayAdapter<bus_stop_item> {

    Context context;
    int layoutResourceId;
    List<bus_stop_item> data = null;

    public CustomAdapterBusStopItem(@NonNull Context context, int resource, @NonNull List<bus_stop_item> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    static class BusStopItem {
        TextView bus_stop_name;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomAdapterBusStopItem.BusStopItem BusStopItem = null;

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            convertView = inflater.inflate(layoutResourceId, parent, false);

            BusStopItem = new CustomAdapterBusStopItem.BusStopItem();
            BusStopItem.bus_stop_name = (TextView)convertView.findViewById(R.id.text_transport_menu_1);

            convertView.setTag(BusStopItem);
        }
        else {
            BusStopItem = (CustomAdapterBusStopItem.BusStopItem)convertView.getTag();
        }

        bus_stop_item dataItem = data.get(position);
        BusStopItem.bus_stop_name.setText(dataItem.bus_stop_name);

        return convertView;
    }
}
