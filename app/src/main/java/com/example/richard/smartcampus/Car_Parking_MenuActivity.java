package com.example.richard.smartcampus;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;


public class Car_Parking_MenuActivity extends AppCompatActivity {

    public static TextView no_car_ParkingTR;
    public static TextView no_car_ParkingIB;
    public static ProgressBar ProgressBar_CarTR;
    public static ProgressBar ProgressBar_CarIB;
    public static Resources res;
    public static LatLng latLng;
    public static String title_position;

    public static TextView time_pred_1;
    public static TextView time_pred_2;
    public static TextView time_pred_3;
    public static TextView val_pred_1;
    public static TextView val_pred_2;
    public static TextView val_pred_3;

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car__parking__menu);

        no_car_ParkingTR = (TextView) findViewById(R.id.text_spot_min_2);
        no_car_ParkingIB = (TextView) findViewById(R.id.text_spot_min_1);

        TextView max_car_ParkingTR = (TextView) findViewById(R.id.text_bar_max_2);
        TextView max_car_ParkingIB = (TextView) findViewById(R.id.text_bar_max_1);

        ProgressBar_CarTR = (ProgressBar) findViewById(R.id.progressBar_2);
        ProgressBar_CarTR.setMax(300);

        ProgressBar_CarIB = (ProgressBar) findViewById(R.id.progressBar_1);
        ProgressBar_CarIB.setMax(200);

        time_pred_1 = (TextView) findViewById(R.id.time_pred_1);
        time_pred_2 = (TextView) findViewById(R.id.time_pred_2);
        time_pred_3 = (TextView) findViewById(R.id.time_pred_3);

        val_pred_1 = (TextView) findViewById(R.id.val_pred_1);
        val_pred_2 = (TextView) findViewById(R.id.val_pred_2);
        val_pred_3 = (TextView) findViewById(R.id.val_pred_3);

        t = new Thread(){

            @Override
            public void run() {
            try{

                while (!isInterrupted())
                {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Fetch_CarParking_Data process_CarParking = new Fetch_CarParking_Data();
                            process_CarParking.execute();
                        }
                    });
                    Thread.sleep(1000*60*3);
                }

            } catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            }
        };
        t.start();


    }

    public void Go_to_Car_Location(View view)
    {
        Intent intent_car_location;

        switch(view.getId())
        {
            case R.id.gps:
                latLng = new LatLng(25.01287, 121.53979);
                title_position = "NTUST IB";
                intent_car_location = new Intent(Car_Parking_MenuActivity.this, Car_LocationActivity.class);
                startActivity(intent_car_location);
                break;
            case R.id.gps2:
                latLng = new LatLng(25.014980, 121.543114);
                title_position = "NTUST TR";
                intent_car_location = new Intent(Car_Parking_MenuActivity.this, Car_LocationActivity.class);
                startActivity(intent_car_location);
                break;
            case R.id.gps3:
                latLng = new LatLng(25.0125374, 121.5378924);
                title_position = "NTU";
                intent_car_location = new Intent(Car_Parking_MenuActivity.this, Car_LocationActivity.class);
                startActivity(intent_car_location);
                break;
        }

    }

    public void toPrevious(View view)
    {
        t.interrupt();
        Intent intent_ParkingMenu = new Intent(this, Parking_MenuActivity.class);
        startActivity(intent_ParkingMenu);
        this.finish();
    }
}
