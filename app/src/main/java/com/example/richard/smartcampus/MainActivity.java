package com.example.richard.smartcampus;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.Image;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class MainActivity extends AppCompatActivity {

    public static TextView txtTemp, txtTempRange, txtLocation, txtStatus, txtLastUpdated;
    public static ImageView imgWeather;
    private FusedLocationProviderClient mFusedLocationClient;
    public static String latitude;
    public static String longitude;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTemp = findViewById(R.id.textViewTemp);
        txtTempRange = findViewById(R.id.textViewTempRange);
        txtLocation = findViewById(R.id.textViewLocation);
        txtStatus = findViewById(R.id.textViewWeatherStatus);
        txtLastUpdated = findViewById(R.id.textViewLastUpdated);
        imgWeather = findViewById(R.id.imageView3);

        // Weather
        mRecyclerView = findViewById(R.id.cardView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        Thread tWeather = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Fetch_Weather_Data().execute();
                            }
                        });
                        Thread.sleep(1000*5*60);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        tWeather.start();
        Thread tWeatherForecast = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                new Fetch_Weather_Forecast_Data(mRecyclerView).execute();
                            }
                        });
                        Thread.sleep(1000*30*60);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        tWeatherForecast.start();

        //Get Current Location
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                }
            }
        });
    }

    public void Go_to_Bus_menu(View view)
    {
        Intent intent_bus_stop_menu = new Intent(MainActivity.this, Transportation_MenuActivity.class);
        startActivity(intent_bus_stop_menu);
    }

    public void Go_to_Parking_menu(View view)
    {
        Intent intent_parking_menu = new Intent(MainActivity.this, Parking_MenuActivity.class);
        startActivity(intent_parking_menu);
    }

    public void Go_to_TrafficVideo_menu(View view)
    {
        Intent intent_traffic_video_menu = new Intent(MainActivity.this, Traffic_Video_MenuActivity.class);
        startActivity(intent_traffic_video_menu);
    }

    public void Go_to_Local_Information_menu(View view)
    {
        Intent intent_local_information_menu = new Intent(MainActivity.this, Local_Information_MenuActivity.class);
        startActivity(intent_local_information_menu);
    }

    public void Go_to_Enviro_menu(View view)
    {
        Intent intent_enviro_menu = new Intent(MainActivity.this, Environment_MenuActivity.class);
        startActivity(intent_enviro_menu);
    }

    public void Go_to_EMS_menu(View view)
    {
        Intent intent_ems_menu = new Intent(MainActivity.this, EMS_MenuActivity.class);
        startActivity(intent_ems_menu);
    }

    public void Go_to_InnerLocation(View view)
    {
        Intent intent_innerLocation_menu = getPackageManager().getLaunchIntentForPackage("com.mitlab.zusliu");
        startActivity(intent_innerLocation_menu);
    }

    public void Go_to_Crowdsourcing(View view)
    {
        Intent intent_innerLocation_menu = getPackageManager().getLaunchIntentForPackage("itai.crowdsourcing.ntust_maintenance");
        startActivity(intent_innerLocation_menu);
    }

}
