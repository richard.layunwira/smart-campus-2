package com.example.richard.smartcampus;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterObjectDetectionItem extends ArrayAdapter<object_detection_item> {

    Context context;
    int layoutResourceId;
    List<object_detection_item> data = null;

    public CustomAdapterObjectDetectionItem(@NonNull Context context, int resource, @NonNull List<object_detection_item> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    static class ObjectDetectionItem {
        TextView object_name;
        TextView object_number;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomAdapterObjectDetectionItem.ObjectDetectionItem objectDetectionItem = null;

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            convertView = inflater.inflate(layoutResourceId, parent, false);

            objectDetectionItem = new CustomAdapterObjectDetectionItem.ObjectDetectionItem();
            objectDetectionItem.object_name = (TextView)convertView.findViewById(R.id.textView7);
            objectDetectionItem.object_number = (TextView)convertView.findViewById(R.id.textView8);

            convertView.setTag(objectDetectionItem);
        }
        else {
            objectDetectionItem = (CustomAdapterObjectDetectionItem.ObjectDetectionItem)convertView.getTag();
        }

        object_detection_item dataItem = data.get(position);
        objectDetectionItem.object_name.setText(dataItem.object_name);
        objectDetectionItem.object_number.setText(dataItem.object_number);

        return convertView;
    }

}
