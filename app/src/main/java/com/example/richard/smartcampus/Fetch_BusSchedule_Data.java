package com.example.richard.smartcampus;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

public class Fetch_BusSchedule_Data extends AsyncTask< Void, Void, Void> {

    String response="";
    String response_2="";
    List<BusN1EstimateTime> obj_1;
    List<BusA2Data> obj_2;
    String APIUrl_1="";
    String APIUrl_2="";

    int index_temp_1;

    String Time_1;
    String Time_2;
    String plat_1;
    String plat_2;
    String currentPosition_1;
    String currentPosition_2;


    @Override
    protected Void doInBackground(Void... voids)
    {
        Time_1="-";
        Time_2="-";
        plat_1="-";
        plat_2="-";
        currentPosition_1="-";
        currentPosition_2="-";

        for(int i=0; i<Fetch_DisplayBusStopRoute_Data.obj_2.size(); i++)
        {
            if(View_Bus_ScheduleActivity.route_id.equals(Fetch_DisplayBusStopRoute_Data.obj_2.get(i).route_uid))
            {
                index_temp_1 = i;
            }
        }

        HttpURLConnection connection=null;
        APIUrl_1 = "http://ptx.transportdata.tw/MOTC/v2/Bus/EstimatedTimeOfArrival/City/Taipei?$filter=RouteUID%20eq%20%27"+ View_Bus_ScheduleActivity.route_id.toString() + "%27%20and%20Direction%20eq%20%27" + Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).direction + "%27&$format=JSON";
        APIUrl_2 = "http://ptx.transportdata.tw/MOTC/v2/Bus/RealTimeNearStop/City/Taipei?$filter=RouteUID%20eq%20%27" + View_Bus_ScheduleActivity.route_id.toString() + "%27%20and%20Direction%20eq%20%27" + Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).direction + "%27&$format=JSON";
        String APPID = "c618894cd2cd4d4099c1cccbd9c4db9c";
        String APPKey = "nHx6HYj-p0FOi4mRd4wdXNvQxpE";

        String xdate = getServerTime();
        String SignDate = "x-date: " + xdate;

        String Signature="";

        try {
            Signature = HMAC_SHA1.Signature(SignDate, APPKey);
        } catch (SignatureException e1)
        {
            e1.printStackTrace();
        }

        String sAuth = "hmac username=\"" + APPID + "\", algorithm=\"hmac-sha1\", headers=\"x-date\", signature=\"" + Signature + "\"";

        try {

            URL url_1 = new URL(APIUrl_1);
            connection = (HttpURLConnection) url_1.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", sAuth);
            connection.setRequestProperty("x-date", xdate);
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setDoInput(true);

            InputStream inputStream = connection.getInputStream();
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buff)) != -1) {
                bao.write(buff, 0, bytesRead);
            }

            ByteArrayInputStream bais = new ByteArrayInputStream(bao.toByteArray());
            GZIPInputStream gzis = new GZIPInputStream(bais);
            InputStreamReader reader = new InputStreamReader(gzis);
            BufferedReader in = new BufferedReader(reader);

            String line="";
            response="";
            while ((line = in.readLine()) != null) {
                response += (line + "\n");
            }

            Type BusEstimateTimeListType = new TypeToken<ArrayList<BusN1EstimateTime>>(){}.getType();
            Gson gsonReceiver_1 = new Gson();
            obj_1 = gsonReceiver_1.fromJson(response, BusEstimateTimeListType);

            URL url_2 = new URL(APIUrl_2);
            connection = (HttpURLConnection) url_2.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", sAuth);
            connection.setRequestProperty("x-date", xdate);
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setDoInput(true);

            InputStream inputStream_2 = connection.getInputStream();
            ByteArrayOutputStream bao_2 = new ByteArrayOutputStream();
            byte[] buff_2 = new byte[1024];
            int bytesRead_2 = 0;
            while ((bytesRead_2 = inputStream_2.read(buff_2)) != -1) {
                bao_2.write(buff_2, 0, bytesRead_2);
            }

            ByteArrayInputStream bais_2 = new ByteArrayInputStream(bao_2.toByteArray());
            GZIPInputStream gzis_2 = new GZIPInputStream(bais_2);
            InputStreamReader reader_2 = new InputStreamReader(gzis_2);
            BufferedReader in_2 = new BufferedReader(reader_2);

            String line_2="";
            response_2="";
            while ((line_2 = in_2.readLine()) != null) {
                response_2 += (line_2 + "\n");
            }

            Type BusA2DataType = new TypeToken<ArrayList<BusA2Data>>(){}.getType();
            Gson gsonReceiver_2 = new Gson();
            obj_2 = gsonReceiver_2.fromJson(response_2, BusA2DataType);


        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        for(int i=0;i<Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.size(); i++)
        {
            for(int j=0; j<obj_1.size(); j++)
            {
                if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).stop_uid.equals(obj_1.get(j).stop_uid))
                {
                    if(obj_1.get(j).estimate_time != null) {
                        Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).setTime(Integer.parseInt(obj_1.get(j).estimate_time) / 60);
                    }
                    else
                    {
                        Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).setTime(-1);
                    }
                }
            }

            for(int j=0; j<obj_2.size();j++)
            {
                if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).stop_uid.equals(obj_2.get(j).stop_uid))
                {
                    Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).setPlate_numb(obj_2.get(j).plate_numb);
                }
            }
        }

        for(int i=0;i<Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.size(); i++)
        {
            if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).stop_name.En.equals("NTUST"))
            {
                if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).getTime() == -1)
                {
                    Time_1 = "Serv over";
                }
                else
                {
                    Time_1 = Integer.toString(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).getTime());

                    int count_temp=1;
                    for(int j=i-1; j>=0; j--)
                    {
                        if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getPlate_numb() != null && count_temp<2)
                        {
                            plat_1 = Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getPlate_numb();
                            if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getTime() < 1)
                            {
                                Time_2 = Integer.toString(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j-1).getTime() + Integer.parseInt(Time_1));
                                currentPosition_1 = Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j-1).stop_name.En;
                            }
                            else
                            {
                                Time_2 = Integer.toString(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getTime() + Integer.parseInt(Time_1));
                                currentPosition_1 = Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).stop_name.En;
                            }
                            count_temp += 1;
                        }
                        else if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getPlate_numb() != null && count_temp==2)
                        {
                            plat_2 = Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).getPlate_numb();
                            currentPosition_2 = Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(j).stop_name.En;
                            break;
                        }
                    }

                }

                if(Time_2.equals("-"))
                {
                    Time_2 = "Serv over";
                }
                break;
            }
        }

        // Reset Plate number in Array
        for(int i=0;i<Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.size(); i++)
        {
            for(int j=0; j<obj_2.size();j++)
            {
                if(Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).stop_uid.equals(obj_2.get(j).stop_uid))
                {
                    Fetch_DisplayBusStopRoute_Data.obj_2.get(index_temp_1).stops.get(i).setPlate_numb(null);
                }
            }
        }

        return null;
    }

    public static String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        if(Time_1.equals("Serv over"))
        {
            View_Bus_ScheduleActivity.listData.add(new bus_schedule_item("Bus 1","-", "-", Time_1));
        }
        else
        {
            View_Bus_ScheduleActivity.listData.add(new bus_schedule_item("Bus 1",plat_1, currentPosition_1, Time_1 + " min"));
        }

        if(Time_2.equals("Serv over"))
        {
            View_Bus_ScheduleActivity.listData.add(new bus_schedule_item("Bus 2","-", "-", Time_2));
        }
        else
        {
            View_Bus_ScheduleActivity.listData.add(new bus_schedule_item("Bus 2",plat_2, currentPosition_2, Time_2 + " min"));
        }

        View_Bus_ScheduleActivity.listView.setAdapter(View_Bus_ScheduleActivity.adapter);

        super.onPostExecute(aVoid);
    }



}
