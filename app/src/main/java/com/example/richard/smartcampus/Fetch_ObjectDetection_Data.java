package com.example.richard.smartcampus;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch_ObjectDetection_Data extends AsyncTask< Void, Void, Void> {

    String data_fetch="";
    private String[] filter;

    JSONArray data_object;

    @Override
    protected Void doInBackground(Void... voids)
    {
        filter = new String[5];
        filter[0] = "bicycle";
        filter[1] = "bus";
        filter[2] = "car";
        filter[3] = "motorbike";
        filter[4] = "person";

        try{
            URL url = new URL("http://140.118.101.215:3000/video/object");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while(line != null)
            {
                data_fetch = data_fetch + line;
                line = bufferedReader.readLine();
            }

            JSONObject root = new JSONObject(data_fetch);
            data_object = root.getJSONArray("data");

        }catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

//        for(int i=0; i<data_object.length(); i++)
//        {
//            try {
//
//                JSONObject temp_data = data_object.getJSONObject(i);
//                String temp = temp_data.getString("name");
//                String val = temp_data.getString("count");
//
//                for (int j=0; j<5; j++)
//                {
//                    if(temp.equals(filter[j]))
//                    {
//                        Traffic_Video_Activity.listData.add(new object_detection_item(temp,val));
//                        break;
//                    }
//                }
//
//            }catch (JSONException e)
//            {
//                e.printStackTrace();
//            }
//        }

        //Traffic_Video_Activity.listData.add(new object_detection_item("asdsadsad","asdasdasd"));
//        Traffic_Video_Activity.listView.setAdapter(Traffic_Video_Activity.adapter);

    }

}
