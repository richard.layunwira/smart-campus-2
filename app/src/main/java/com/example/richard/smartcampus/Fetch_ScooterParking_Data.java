package com.example.richard.smartcampus;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch_ScooterParking_Data  extends AsyncTask< Void, Void, Void> {

    String data_ParkingTR = "";
    String no_scooter_ParkingTR = "";

    @Override
    protected Void doInBackground(Void... voids)
    {
        try{
            URL url_ParkingTR = new URL("http://140.118.101.189:4000/parking-elastic/data");
            HttpURLConnection httpURLConnection_ParkingTR = (HttpURLConnection) url_ParkingTR.openConnection();
            InputStream inputStream_ParkingTR = httpURLConnection_ParkingTR.getInputStream();
            BufferedReader bufferedReader_ParkingTR = new BufferedReader(new InputStreamReader(inputStream_ParkingTR));
            String line_ParkingTR = "";

            while(line_ParkingTR != null)
            {
                data_ParkingTR = data_ParkingTR + line_ParkingTR;
                line_ParkingTR = bufferedReader_ParkingTR.readLine();
            }

            JSONObject root_ParkingTR = new JSONObject(data_ParkingTR);
            JSONObject hits_ParkingTR = root_ParkingTR.getJSONObject("hits");
            JSONArray hits_array_ParkingTR = hits_ParkingTR.getJSONArray("hits");
            JSONObject temp_ParkingTR = hits_array_ParkingTR.getJSONObject(0);
            JSONObject source_ParkingTR = temp_ParkingTR.getJSONObject("_source");
            no_scooter_ParkingTR = source_ParkingTR.getString("bikeinpark");

            httpURLConnection_ParkingTR.disconnect();

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        Scooter_Parking_MenuActivity.no_scooter_ParkingTR.setText(Integer.toString(900-Integer.parseInt(this.no_scooter_ParkingTR)));
        Scooter_Parking_MenuActivity.ProgressBar_ScooterTR.setProgress(Integer.parseInt(this.no_scooter_ParkingTR));
    }
}
