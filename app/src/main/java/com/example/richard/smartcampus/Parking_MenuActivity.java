package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Parking_MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking__menu);
    }

    public void Go_to_Scooter_Parking_List(View view)
    {
        Intent intent_scooter_parking_list = new Intent(Parking_MenuActivity.this, Scooter_Parking_MenuActivity.class);
        startActivity(intent_scooter_parking_list);
    }

    public void Go_to_Car_Parking_List(View view)
    {
        Intent intent_car_parking_list = new Intent(Parking_MenuActivity.this, Car_Parking_MenuActivity.class);
        startActivity(intent_car_parking_list);
    }

    public void toPrevious(View view)
    {
        Intent intent_MainActivity = new Intent(this, MainActivity.class);
        startActivity(intent_MainActivity);
    }
}
