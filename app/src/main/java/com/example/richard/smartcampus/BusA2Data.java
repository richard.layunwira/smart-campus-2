package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

public class BusA2Data {

    @SerializedName("StopUID")
    public String stop_uid;

    @SerializedName("PlateNumb")
    public String plate_numb;

    public void setStop_UID(String stop_uid){
        this.stop_uid = stop_uid;
    }

    public void setPlate_Numb(String plate_numb){
        this.plate_numb = plate_numb;
    }

}
