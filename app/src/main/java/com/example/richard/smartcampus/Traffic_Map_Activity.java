package com.example.richard.smartcampus;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;

public class Traffic_Map_Activity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    double latitude, longitude;

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic__map_);

        // textView
        TextView textView = findViewById(R.id.textViewTrafficItem);
        textView.setText(getIntent().getStringExtra("VideoName"));

        // latitudeLongitude
        latitude = Double.parseDouble(getIntent().getStringExtra("VideoLatitude"));
        longitude = Double.parseDouble(getIntent().getStringExtra("VideoLongitude"));

        // googleMaps
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // dateTime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = findViewById(R.id.textViewDate);
                                TextView tHour = findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    //marker
    private BitmapDescriptor bitmapDescriptorFromVector(Context context) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_location_marker);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, R.drawable.ic_video_icon);
        vectorDrawable.setBounds(19, 19, vectorDrawable.getIntrinsicWidth() + 19, vectorDrawable.getIntrinsicHeight() + 19);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng currentLocation = new LatLng(latitude,longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentLocation);
        markerOptions.title(getIntent().getStringExtra("VideoName"));
        markerOptions.icon(bitmapDescriptorFromVector(this));
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 19.75f));
    }

    public void toPrevious(View view)
    {
        t.interrupt();

        Intent intent = new Intent(this, Traffic_Video_Activity.class);
        intent.putExtra("VideoName", getIntent().getStringExtra("VideoName"));
        intent.putExtra("VideoAddress", getIntent().getStringExtra("VideoAddress"));
        intent.putExtra("VideoLatitude", getIntent().getStringExtra("VideoLatitude"));
        intent.putExtra("VideoLongitude", getIntent().getStringExtra("VideoLongitude"));
        startActivity(intent);

        this.finish();
    }
}
