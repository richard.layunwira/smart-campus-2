package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Stops {
    
    @SerializedName("StopUID")
    public String stop_uid;

    @SerializedName("StopName")
    public StopName stop_name;

    public int data_time;

    public String plate_numb;

    public void setStop_UID(String stop_uid){
        this.stop_uid = stop_uid;
    }

    public void setStop_Name(StopName stop_name) { this.stop_name = stop_name; }

    public void setTime(int data_time) {this.data_time = data_time;}

    public int getTime() {return this.data_time;}

    public void setPlate_numb(String plate_numb) {this.plate_numb = plate_numb;}

    public String getPlate_numb() {return this.plate_numb;}
}
