package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BusDisplayStopOfRoute {

    @SerializedName("RouteUID")
    public String route_uid;

    @SerializedName("RouteName")
    public RouteName route_name;

    @SerializedName("Stops")
    public List<Stops> stops;

    @SerializedName("Direction")
    public String direction;

    public void setRoute_UID(String route_uid){
        this.route_uid = route_uid;
    }

    public void setRouteName(RouteName RouteName){
        this.route_name = RouteName;
    }

    public void setStops(List<Stops> stops){
        this.stops = stops;
    }

    public void setDirection(String direction){
        this.direction = direction;
    }

}
