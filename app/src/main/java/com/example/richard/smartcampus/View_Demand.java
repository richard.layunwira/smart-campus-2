package com.example.richard.smartcampus;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class View_Demand extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__demand);

        LineChart linechart_demand;

        linechart_demand = (LineChart) findViewById(R.id.chart_demand);
        linechart_demand.getDescription().setEnabled(false);

        ArrayList<Entry> lineEntries_demand_1 = new ArrayList<>();
        lineEntries_demand_1.add(new Entry(0, 178.75f));
        lineEntries_demand_1.add(new Entry(1, 339.81f));
        lineEntries_demand_1.add(new Entry(2, 405.97f));
        lineEntries_demand_1.add(new Entry(3, 424.65f));
        lineEntries_demand_1.add(new Entry(4, 428.30f));
        lineEntries_demand_1.add(new Entry(5, 417.23f));
        lineEntries_demand_1.add(new Entry(6, 431.95f));

        LineDataSet lineDataSet_1 = new LineDataSet(lineEntries_demand_1, "Demand(kW)");
        lineDataSet_1.setColor(Color.parseColor("#f0ad4e"));
        lineDataSet_1.setLineWidth(2f);

        ArrayList<Entry> lineEntries_demand_2 = new ArrayList<>();
        lineEntries_demand_2.add(new Entry(6, 431.95f));
        lineEntries_demand_2.add(new Entry(7, 437.25f));
        lineEntries_demand_2.add(new Entry(8, 430.12f));
        lineEntries_demand_2.add(new Entry(9, 413.81f));
        lineEntries_demand_2.add(new Entry(10, 381.71f));
        lineEntries_demand_2.add(new Entry(11, 372.64f));

        LineDataSet lineDataSet_2 = new LineDataSet(lineEntries_demand_2, "Prediction(kW)");
        lineDataSet_2.setColor(Color.parseColor("#3bc64e"));
        lineDataSet_2.setLineWidth(2f);

        LineData data = new LineData(lineDataSet_1, lineDataSet_2);
        linechart_demand.setData(data);

        final ArrayList<String> xlabel = new ArrayList<>();
        xlabel.add("07:00");
        xlabel.add("08:00");
        xlabel.add("09:00");
        xlabel.add("10:00");
        xlabel.add("11:00");
        xlabel.add("12:00");
        xlabel.add("13:00");
        xlabel.add("14:00");
        xlabel.add("15:00");
        xlabel.add("16:00");
        xlabel.add("17:00");
        xlabel.add("18:00");

        XAxis xAxis = linechart_demand.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xlabel.get((int)value);
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1);

        YAxis rightAxis = linechart_demand.getAxisRight();
        rightAxis.setEnabled(false);

        Legend legend = linechart_demand.getLegend();
        legend.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
        legend.setTextSize(12);
    }
}
