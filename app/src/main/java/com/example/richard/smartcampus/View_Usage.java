package com.example.richard.smartcampus;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class View_Usage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__usage);

        LineChart linechart_usage;

        linechart_usage = (LineChart) findViewById(R.id.chart_usage);
        linechart_usage.getDescription().setEnabled(false);

        ArrayList<Entry> lineEntries_usage_1 = new ArrayList<>();
        lineEntries_usage_1.add(new Entry(0, 168.22f));
        lineEntries_usage_1.add(new Entry(1, 313.48f));
        lineEntries_usage_1.add(new Entry(2, 424.01f));
        lineEntries_usage_1.add(new Entry(3, 521.75f));
        lineEntries_usage_1.add(new Entry(4, 524.55f));
        lineEntries_usage_1.add(new Entry(5, 540.41f));
        lineEntries_usage_1.add(new Entry(6, 530.39f));

        LineDataSet lineDataSet_1 = new LineDataSet(lineEntries_usage_1, "Usage(kWh)");
        lineDataSet_1.setColor(Color.parseColor("#337ab7"));
        lineDataSet_1.setLineWidth(2f);

        ArrayList<Entry> lineEntries_usage_2 = new ArrayList<>();
        lineEntries_usage_2.add(new Entry(6, 530.39f));
        lineEntries_usage_2.add(new Entry(7, 567.31f));
        lineEntries_usage_2.add(new Entry(8, 572f));
        lineEntries_usage_2.add(new Entry(9, 553.7f));
        lineEntries_usage_2.add(new Entry(10, 521.03f));
        lineEntries_usage_2.add(new Entry(11, 444.93f));

        LineDataSet lineDataSet_2 = new LineDataSet(lineEntries_usage_2, "Prediction(kWh)");
        lineDataSet_2.setColor(Color.parseColor("#3bc64e"));
        lineDataSet_2.setLineWidth(2f);

        LineData data = new LineData(lineDataSet_1, lineDataSet_2);
        linechart_usage.setData(data);

        final ArrayList<String> xlabel = new ArrayList<>();
        xlabel.add("07:00");
        xlabel.add("08:00");
        xlabel.add("09:00");
        xlabel.add("10:00");
        xlabel.add("11:00");
        xlabel.add("12:00");
        xlabel.add("13:00");
        xlabel.add("14:00");
        xlabel.add("15:00");
        xlabel.add("16:00");
        xlabel.add("17:00");
        xlabel.add("18:00");

        XAxis xAxis = linechart_usage.getXAxis();
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xlabel.get((int)value);
            }
        });
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1);

        YAxis rightAxis = linechart_usage.getAxisRight();
        rightAxis.setEnabled(false);

        Legend legend = linechart_usage.getLegend();
        legend.setPosition(Legend.LegendPosition.ABOVE_CHART_CENTER);
        legend.setTextSize(12);
    }
}
