package com.example.richard.smartcampus;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterTrafficVideoItem extends ArrayAdapter<traffic_video_item> {

    Context context;
    int layoutResourceId;
    List<traffic_video_item> data = null;

    public CustomAdapterTrafficVideoItem(@NonNull Context context, int resource, @NonNull List<traffic_video_item> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    static class DataHolder {
        TextView tvVideoName;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        DataHolder holder = null;

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            convertView = inflater.inflate(layoutResourceId, parent, false);

            holder = new DataHolder();
            holder.tvVideoName = convertView.findViewById(R.id.textViewVideoName);

            convertView.setTag(holder);
        }
        else {
            holder = (DataHolder)convertView.getTag();
        }

        traffic_video_item dataItem = data.get(position);
        holder.tvVideoName.setText(dataItem.videoName);

        return convertView;
    }

}
