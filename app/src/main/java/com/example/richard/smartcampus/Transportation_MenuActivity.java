package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Transportation_MenuActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    List<bus_stop_item> listData;
    public static String option_marker="";
    private GoogleMap GMap;
    private Marker marker_1;
    private Marker marker_2;
    private Marker marker_3;
    private Marker marker_4;
    private Marker marker_5;

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transportation__menu);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.viewBusStation);
        mapFragment.getMapAsync(this);

        //List View
        listData = new ArrayList<>();
        listData.add(new bus_stop_item("NTUST\nSouth West"));
        listData.add(new bus_stop_item("NTUST\nNorth East"));
        listData.add(new bus_stop_item("Gongguan\nSouth West"));
        listData.add(new bus_stop_item("Gongguan\nNorth East"));
        listData.add(new bus_stop_item("NTU Gongguan Hospital\nSouth West"));
        //listData.add(new bus_stop_item("NTU Gongguan Hospital\nNorth East"));

        ListView listView = (ListView)findViewById(R.id.list_bus_stop);
        CustomAdapterBusStopItem adapter = new CustomAdapterBusStopItem(this, R.layout.list_item_bus_stop, listData);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("bus_stop_name", listData.get(position).bus_stop_name);

                intent.setClass(Transportation_MenuActivity.this, View_Bus_RouteActivity.class);
                startActivity(intent);
            }
        });

        //Datetime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = (TextView) findViewById(R.id.textViewDate);
                                TextView tHour = (TextView) findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    public void onMapReady(GoogleMap googleMap) {
        GMap = googleMap;

        GMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng Location_1 = new LatLng(25.0134823,121.5402836);
        LatLng Location_2 = new LatLng(25.0141878,121.5404512);
        LatLng Location_3 = new LatLng(25.0120938,121.5385509);
        LatLng Location_4 = new LatLng(25.0121783,121.5380171);
        LatLng Location_5 = new LatLng(25.015357,121.5423958);

        googleMap.setOnMarkerClickListener(this);

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Location_1, 16.0f));

        markerOptions.position(Location_1);
        markerOptions.title("NTUST South West");
        marker_1 = googleMap.addMarker(markerOptions);

        markerOptions.position(Location_2);
        markerOptions.title("NTUST North East");
        marker_2 = googleMap.addMarker(markerOptions);

        markerOptions.position(Location_3);
        markerOptions.title("Gongguan South West");
        marker_3 = googleMap.addMarker(markerOptions);

        markerOptions.position(Location_4);
        markerOptions.title("Gongguan North East");
        marker_4 = googleMap.addMarker(markerOptions);

        markerOptions.position(Location_5);
        markerOptions.title("NTU Gongguan Hospital\nSouth West");
        marker_5 = googleMap.addMarker(markerOptions);

    }

    public boolean onMarkerClick (Marker marker) {
        t.interrupt();

        if(marker.equals(marker_1))
        {
            option_marker = "NTUST\nSouth West";
            Intent intent = new Intent(this, View_Bus_RouteActivity.class);
            startActivity(intent);
            return true;
        }
        else if(marker.equals(marker_2))
        {
            option_marker = "NTUST\nNorth East";
            Intent intent = new Intent(this, View_Bus_RouteActivity.class);
            startActivity(intent);
            return true;
        }
        else if(marker.equals(marker_3))
        {
            option_marker = "Gongguan\nSouth West";
            Intent intent = new Intent(this, View_Bus_RouteActivity.class);
            startActivity(intent);
            return true;
        }
        else if(marker.equals(marker_4))
        {
            option_marker = "Gongguan\nNorth East";
            Intent intent = new Intent(this, View_Bus_RouteActivity.class);
            startActivity(intent);
            return true;
        }
        else if(marker.equals(marker_5))
        {
            option_marker = "NTU Gongguan Hospital\nSouth West";
            Intent intent = new Intent(this, View_Bus_RouteActivity.class);
            startActivity(intent);
            return true;
        }
        else
        {
            return false;
        }

    }

    public void toPrevious(View view)
    {
        t.interrupt();

        Intent intent_MainActivity = new Intent(this, MainActivity.class);
        startActivity(intent_MainActivity);

        this.finish();
    }
}
