package com.example.richard.smartcampus;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;

public class Fetch_DisplayBusStopRoute_Data extends AsyncTask< Void, Void, Void> {

    String response="";
    String response_2="";
    List<BusStop> obj_1;
    String APIUrl_1="";
    String APIUrl_2="";
    String stop_list = "";

    public static List<BusDisplayStopOfRoute> obj_2;

    @Override
    protected Void doInBackground(Void... voids)
    {
        HttpURLConnection connection=null;
        APIUrl_1 = View_Bus_RouteActivity.APIUrl;
        String APPID = "c618894cd2cd4d4099c1cccbd9c4db9c";
        String APPKey = "nHx6HYj-p0FOi4mRd4wdXNvQxpE";

        String xdate = getServerTime();
        String SignDate = "x-date: " + xdate;

        String Signature="";

        try {
            Signature = HMAC_SHA1.Signature(SignDate, APPKey);
        } catch (SignatureException e1)
        {
            e1.printStackTrace();
        }

        String sAuth = "hmac username=\"" + APPID + "\", algorithm=\"hmac-sha1\", headers=\"x-date\", signature=\"" + Signature + "\"";

        try{

            URL url_1 = new URL(APIUrl_1);
            connection =(HttpURLConnection) url_1.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", sAuth);
            connection.setRequestProperty("x-date", xdate);
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setDoInput(true);

            InputStream inputStream = connection.getInputStream();
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            byte[] buff = new byte[1024];
            int bytesRead = 0;
            while((bytesRead = inputStream.read(buff)) != -1) {
                bao.write(buff, 0, bytesRead);
            }

            ByteArrayInputStream bais = new ByteArrayInputStream(bao.toByteArray());
            GZIPInputStream gzis = new GZIPInputStream(bais);
            InputStreamReader reader = new InputStreamReader(gzis);
            BufferedReader in = new BufferedReader(reader);

            String line;
            while ((line = in.readLine()) != null) {
                response+=(line+"\n");
            }

            Type BusStopListType = new TypeToken<ArrayList<BusStop>>(){}.getType();
            Gson gsonReceiver_1 = new Gson();
            obj_1= gsonReceiver_1.fromJson(response, BusStopListType);


            for (int i=0; i<obj_1.size(); i++)
            {
                if (i == obj_1.size()-1)
                {
                    stop_list += "Stops/any(d:(contains(d/StopUID,%20%27" + obj_1.get(i).stop_uid.toString() + "%27)%20eq%20true))";
                }
                else
                {
                    stop_list += "Stops/any(d:(contains(d/StopUID,%20%27" + obj_1.get(i).stop_uid.toString() + "%27)%20eq%20true))%20or%20";
                }
            }

            APIUrl_2 = "http://ptx.transportdata.tw/MOTC/v2/Bus/DisplayStopOfRoute/City/Taipei?$filter="+stop_list+"&$format=JSON";

            URL url_2 = new URL(APIUrl_2);
            connection = (HttpURLConnection) url_2.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Authorization", sAuth);
            connection.setRequestProperty("x-date", xdate);
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setDoInput(true);

            InputStream inputStream_2 = connection.getInputStream();
            ByteArrayOutputStream bao_2 = new ByteArrayOutputStream();
            byte[] buff_2 = new byte[1024];
            int bytesRead_2 = 0;
            while((bytesRead_2 = inputStream_2.read(buff_2)) != -1) {
                bao_2.write(buff_2, 0, bytesRead_2);
            }

            ByteArrayInputStream bais_2 = new ByteArrayInputStream(bao_2.toByteArray());
            GZIPInputStream gzis_2 = new GZIPInputStream(bais_2);
            InputStreamReader reader_2= new InputStreamReader(gzis_2);
            BufferedReader in_2 = new BufferedReader(reader_2);

            String line_2;
            while ((line_2 = in_2.readLine()) != null) {
                response_2 += (line_2+"\n");
            }

            Type BusDisplayStopOfRouteListType = new TypeToken<ArrayList<BusDisplayStopOfRoute>>(){}.getType();
            Gson gsonReceiver_2 = new Gson();
            obj_2 = gsonReceiver_2.fromJson(response_2, BusDisplayStopOfRouteListType);

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } finally {
            connection.disconnect();
        }

        return null;
    }

    public static String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        for (int i=0;i<obj_2.size(); i++)
        {
            int size_destination = obj_2.get(i).stops.size();
            String temp_destination = "From " + obj_2.get(i).stops.get(0).stop_name.En + " - To " + obj_2.get(i).stops.get(size_destination-1).stop_name.En;
            View_Bus_RouteActivity.listData.add(new bus_route_item(obj_2.get(i).route_name.En, temp_destination, obj_2.get(i).route_uid));
        }

        View_Bus_RouteActivity.listView.setAdapter(View_Bus_RouteActivity.adapter);
        super.onPostExecute(aVoid);
    }

}
