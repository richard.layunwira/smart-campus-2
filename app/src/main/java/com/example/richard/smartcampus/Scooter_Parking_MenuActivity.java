package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class Scooter_Parking_MenuActivity extends AppCompatActivity {

    public static TextView no_scooter_ParkingTR;
    public static  ProgressBar ProgressBar_ScooterTR;
    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scooter__parking__menu);

        no_scooter_ParkingTR = (TextView) findViewById(R.id.text_spot_min_1);
        TextView max_scooter_ParkingTR = (TextView) findViewById(R.id.text_bar_max_1);
        ProgressBar_ScooterTR = (ProgressBar) findViewById(R.id.progressBar_1);
        ProgressBar_ScooterTR.setMax(900);

        t = new Thread(){

            @Override
            public void run() {
                try{

                    while (!isInterrupted())
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Fetch_ScooterParking_Data process_ScooterParking = new Fetch_ScooterParking_Data();
                                process_ScooterParking.execute();
                            }
                        });
                        Thread.sleep(1000);
                    }

                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        };
        t.start();

    }

    public void Go_to_Scooter_Location(View view)
    {
        t.interrupt();
        Intent intent_scooter_location = new Intent(Scooter_Parking_MenuActivity.this, Scooter_LocationActivity.class);
        startActivity(intent_scooter_location);
        this.finish();
    }

    public void toPrevious(View view)
    {
        t.interrupt();
        Intent intent_ParkingMenu = new Intent(this, Parking_MenuActivity.class);
        startActivity(intent_ParkingMenu);
        this.finish();

    }
}
