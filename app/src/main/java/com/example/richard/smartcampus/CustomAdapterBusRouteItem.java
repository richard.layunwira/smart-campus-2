package com.example.richard.smartcampus;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterBusRouteItem extends ArrayAdapter<bus_route_item> {

    Context context;
    int layoutResourceId;
    List<bus_route_item> data = null;

    public CustomAdapterBusRouteItem(@NonNull Context context, int resource, @NonNull List<bus_route_item> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    static class BusRouteItem {
        TextView bus_route;
        TextView bus_destination;
        TextView bus_route_no;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomAdapterBusRouteItem.BusRouteItem BusRouteItem = null;

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            convertView = inflater.inflate(layoutResourceId, parent, false);

            BusRouteItem = new CustomAdapterBusRouteItem.BusRouteItem();
            BusRouteItem.bus_route = (TextView)convertView.findViewById(R.id.Bus_Route_Id);
            BusRouteItem.bus_destination = (TextView)convertView.findViewById(R.id.Bus_Destination_Id);
            BusRouteItem.bus_route_no = (TextView)convertView.findViewById(R.id.route_no);

            convertView.setTag(BusRouteItem);
        }
        else {
            BusRouteItem = (CustomAdapterBusRouteItem.BusRouteItem)convertView.getTag();
        }

        bus_route_item dataItem = data.get(position);
        BusRouteItem.bus_route.setText(dataItem.bus_route_name);
        BusRouteItem.bus_destination.setText(dataItem.bus_destination);
        BusRouteItem.bus_route_no.setText(dataItem.bus_route_id);

        return convertView;
    }

}
