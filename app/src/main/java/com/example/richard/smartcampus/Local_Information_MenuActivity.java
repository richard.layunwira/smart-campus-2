package com.example.richard.smartcampus;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

public class Local_Information_MenuActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient client;
    private LocationRequest locationRequest;
    private Location lastlocation;
    private Marker currentLocationMarker;

    public static final int REQUEST_LOCATION_CODE = 99;
    int PROXIMITY_RADIUS = 500;
    double latitude, longitude;

    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local__information__menu);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            checkLocationPermission();
        }
        //Google Maps
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //SearchView
        searchView = findViewById(R.id.searchViewLocation);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                mMap.clear();
                dataTransfer(""+query, searchView, R.drawable.ic_custom_search_icon);
                searchView.clearFocus();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode)
        {
            case REQUEST_LOCATION_CODE:
                if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=  PackageManager.PERMISSION_GRANTED)
                    {
                        if(client == null)
                        {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }
                else
                {
                    Toast.makeText(this,"Permission Denied" , Toast.LENGTH_LONG).show();
                }
        }
    }

    private boolean checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)  != PackageManager.PERMISSION_GRANTED )
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION))
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            else
            {
                ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.ACCESS_FINE_LOCATION },REQUEST_LOCATION_CODE);
            }
            return false;
        }
        else
            return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            // mMap.setMyLocationEnabled(true);
        }

        // mMap.setPadding(0,265,0,0);
    }

    protected synchronized void buildGoogleApiClient() {
        client = new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
        client.connect();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED)
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
        lastlocation = location;
        if(currentLocationMarker != null)
        {
            currentLocationMarker.remove();
        }
        Log.d("lat = ",""+latitude);
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomBy(10));

        if(client != null)
        {
            LocationServices.FusedLocationApi.removeLocationUpdates(client,this);
        }
    }

    private void dataTransfer(String locName, View v, int icon)
    {
        Object data[] = new Object[5];
        GetNearbyPlacesData getNearbyPlacesData = new GetNearbyPlacesData();
        LatLng latLng = new LatLng(latitude, longitude);
        Log.d("current", latLng.toString());

        String url = getUrl(latitude, longitude, locName);
        data[0] = mMap;
        data[1] = url;
        data[2] = v;
//        Log.d("view", "this view = "+v.toString());
        data[3] = icon;
        data[4] = latLng;

        getNearbyPlacesData.execute(data);
        Toast.makeText(Local_Information_MenuActivity.this, "Showing Nearby "+locName, Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.buttonSearchRestaurant:
                dataTransfer("restaurant", v, R.drawable.ic_restaurant_icon);
                break;
            case R.id.buttonSearchCafe:
                dataTransfer("cafe", v, R.drawable.ic_cafe_icon);
                break;
            case R.id.buttonSearchPizza:
                dataTransfer("pizza", v, R.drawable.ic_pizza_icon);
                break;
            case R.id.buttonSearchBarber:
                dataTransfer("barber", v, R.drawable.ic_barber_icon);
                break;
            case R.id.buttonSearchCoffee:
                dataTransfer("coffee", v, R.drawable.ic_coffee_icon);
                break;
            case R.id.buttonSearchAtm:
                dataTransfer("ATM", v, R.drawable.ic_atm_icon);
                break;
            case R.id.buttonSearchGas:
                dataTransfer("gas station", v, R.drawable.ic_gas_icon);
                break;
            case R.id.buttonSearchTea:
                dataTransfer("tea", v, R.drawable.ic_tea_icon);
                break;
            case R.id.buttonSearchHalal:
                dataTransfer("halal food", v, R.drawable.ic_halal_icon);
                break;
            case R.id.buttonSearchBank:
                dataTransfer("bank", v, R.drawable.ic_bank_icon);
                break;
            case R.id.buttonSearchDoctor:
                dataTransfer("doctor", v, R.drawable.ic_doctor_icon);
                break;
            case R.id.buttonSearchHospital:
                dataTransfer("hospital", v, R.drawable.ic_hospital_icon);
                break;
        }
    }

    private String getUrl(double latitude , double longitude , String nearbyPlace)
    {
        StringBuilder googlePlaceUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        googlePlaceUrl.append("location="+latitude+","+longitude);
        googlePlaceUrl.append("&radius="+PROXIMITY_RADIUS);
        googlePlaceUrl.append("&keyword="+nearbyPlace);
        googlePlaceUrl.append("&sensor=true");
        googlePlaceUrl.append("&key="+"AIzaSyDjOTh9jagu3Dwdm_VcPGwBphfrmea5RNM");

        Log.d("MapsActivity", "url = "+googlePlaceUrl.toString());

        return googlePlaceUrl.toString();
    }

    public void toPrevious(View view)
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
