package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class View_Bus_RouteActivity extends AppCompatActivity {

    public static List<bus_route_item> listData;
    public static CustomAdapterBusRouteItem adapter;
    public static ListView listView;
    public static String APIUrl="";
    public static String direction_route = "";

    private String Option="";

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__bus__route);

        if(getIntent().hasExtra("bus_stop_name"))
        {
            Option = getIntent().getStringExtra("bus_stop_name").toString();
        }


        if( Option.equals("NTUST\nSouth West") || Transportation_MenuActivity.option_marker.equals("NTUST\nSouth West"))
        {
            this.direction_route = "1";
            this.APIUrl = "http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27NTUST%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF%E5%9B%9B%E6%AE%B575%E8%99%9F%E5%90%8C%E5%90%91(%E5%90%91%E8%A5%BF)%27&$format=JSON";
        }
        else if(Option.equals("NTUST\nNorth East") || Transportation_MenuActivity.option_marker.equals("NTUST\nNorth East"))
        {
            this.direction_route = "0";
            this.APIUrl = "http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27NTUST%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF%E5%9B%9B%E6%AE%B543%E8%99%9F%E5%B0%8D%E9%9D%A2(155%E5%B7%B7%E5%8F%A3%E5%B0%8D%E9%9D%A2)(%E5%90%91%E6%9D%B1)%27&$format=JSON";
        }
        else if(Option.equals("Gongguan\nSouth West") || Transportation_MenuActivity.option_marker.equals("Gongguan\nSouth West"))
        {
            this.direction_route = "1";
            this.APIUrl = "http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27Gongguan%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF%E4%BA%8C%E6%AE%B5%E5%8F%B0%E9%9B%BB%E7%87%9F%E6%A5%AD%E8%99%95%E5%B0%8D%E9%9D%A2%27&$format=JSON";
        }
        else if(Option.equals("Gongguan\nNorth East") || Transportation_MenuActivity.option_marker.equals("Gongguan\nNorth East"))
        {
            this.direction_route = "0";
            this.APIUrl = "http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27Gongguan%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF%E5%9B%9B%E6%AE%B575%E8%99%9F%E8%A5%BF%E5%81%B4%27&$format=JSON";
        }
        else if(Option.equals("NTU Gongguan Hospital\nSouth West") || Transportation_MenuActivity.option_marker.equals("NTU Gongguan Hospital\nSouth West"))
        {
            this.direction_route = "1";
            this.APIUrl = "http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27NTU%20Hospital%20Gongguan%20Branch%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF%E4%B8%89%E6%AE%B5115%E5%B7%B7%E5%90%8C%E5%90%91%27&$format=JSON";
        }
        /*else if(Option.equals("NTU Gongguan Hospital\nNorth East"))
        {
            this.direction_route = "1";
            this.APIUrl="http://ptx.transportdata.tw/MOTC/v2/Bus/Stop/City/Taipei?$filter=StopName/En%20eq%20%27NTU%20Hospital%20Gongguan%20Branch%27%20and%20StopAddress%20eq%20%27%E5%9F%BA%E9%9A%86%E8%B7%AF3%E6%AE%B5155%E5%B7%B7%E5%8F%A3%27&$format=JSON";
        }*/


        //List View
        listData = new ArrayList<>();

        listView = (ListView)findViewById(R.id.list_bus_route);
        adapter = new CustomAdapterBusRouteItem(this, R.layout.list_item_bus_route, listData);

        Fetch_DisplayBusStopRoute_Data process_BusRoute = new Fetch_DisplayBusStopRoute_Data();
        process_BusRoute.execute();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("bus_route_id", listData.get(position).bus_route_id);

                intent.setClass(View_Bus_RouteActivity.this, View_Bus_ScheduleActivity.class);
                startActivity(intent);
            }
        });

        //Datetime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = (TextView) findViewById(R.id.textViewDate);
                                TextView tHour = (TextView) findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    public void toPrevious(View view)
    {
        t.interrupt();

        Intent intent_BusMenu = new Intent(this, Transportation_MenuActivity.class);
        startActivity(intent_BusMenu);

        this.finish();
    }
}
