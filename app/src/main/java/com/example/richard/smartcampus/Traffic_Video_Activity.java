package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
//import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
//import java.util.ArrayList;
import java.util.List;

public class Traffic_Video_Activity extends AppCompatActivity {

    WebView webView;
    public static List<object_detection_item> listData;
    public static CustomAdapterObjectDetectionItem adapter;
//    public static ListView listView;

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic__video_);

        // textView
        TextView textView = findViewById(R.id.textViewTrafficItem);
        textView.setText(getIntent().getStringExtra("VideoName"));

        // webView
        String url = "http://"+getIntent().getStringExtra("VideoAddress")+"/img/video.mjpeg";
        webView = this.findViewById(R.id.webViewVideo);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(url);

        Log.d("Video url", url);

        String addr = getIntent().getStringExtra("VideoAddress");

//        listData = new ArrayList<>();

//        listView = (ListView)findViewById(R.id.list_object);
//        adapter = new CustomAdapterObjectDetectionItem(this, R.layout.list_item_object_detection, listData);

        //Datetime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = findViewById(R.id.textViewDate);
                                TextView tHour = findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

//        if(addr.equals("140.118.17.105"))
//        {
//            t2 = new Thread() {
//                @Override
//                public void run() {
//                    try {
//                        while (!isInterrupted()) {
//
//                            runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    adapter.clear();
//                                    Fetch_ObjectDetection_Data process_ObjectDetection = new Fetch_ObjectDetection_Data();
//                                    process_ObjectDetection.execute();
//                                }
//                            });
//
//                            Thread.sleep(1000*10);
//                        }
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//            };
//            t2.start();
//        }


    }

    public void OpenVideoMap(View view) {
        t.interrupt();

        webView.stopLoading();
        Intent intent = new Intent(this, Traffic_Map_Activity.class);
        intent.putExtra("VideoName", getIntent().getStringExtra("VideoName"));
        intent.putExtra("VideoAddress", getIntent().getStringExtra("VideoAddress"));
        intent.putExtra("VideoLatitude", getIntent().getStringExtra("VideoLatitude"));
        intent.putExtra("VideoLongitude", getIntent().getStringExtra("VideoLongitude"));
        startActivity(intent);

        this.finish();
    }

    public void toPrevious(View view)
    {
        t.interrupt();

        webView.stopLoading();
        Intent intent = new Intent(this, Traffic_Video_MenuActivity.class);
        startActivity(intent);

        this.finish();
    }

}
