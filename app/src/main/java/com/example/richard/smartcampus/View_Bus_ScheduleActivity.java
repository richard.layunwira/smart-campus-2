package com.example.richard.smartcampus;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class View_Bus_ScheduleActivity extends AppCompatActivity {

    public static List<bus_schedule_item> listData;
    public static CustomAdapterBusScheduleItem adapter;
    public static ListView listView;

    public static String route_id;

    public Thread t, t2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__bus__schedule);

        route_id = getIntent().getStringExtra("bus_route_id").toString();

        listData = new ArrayList<>();

        listView = (ListView)findViewById(R.id.list_bus_schedule);
        adapter = new CustomAdapterBusScheduleItem(this, R.layout.list_item_bus_schedule, listData);

        //Fetch_BusSchedule_Data process_BusSchedule = new Fetch_BusSchedule_Data();
        //process_BusSchedule.execute();

        //Datetime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = (TextView) findViewById(R.id.textViewDate);
                                TextView tHour = (TextView) findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                        Thread.sleep(10);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();

        t2 = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.clear();
                                Fetch_BusSchedule_Data process_BusSchedule = new Fetch_BusSchedule_Data();
                                process_BusSchedule.execute();
                            }
                        });
                        Thread.sleep(50000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t2.start();
    }

    public void toPrevious(View view)
    {
        t.interrupt();
        t2.interrupt();

        Intent intent_BusMenu = new Intent(this, Transportation_MenuActivity.class);
        startActivity(intent_BusMenu);

        this.finish();
    }

}
