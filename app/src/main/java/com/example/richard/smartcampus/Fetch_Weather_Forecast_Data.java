package com.example.richard.smartcampus;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Fetch_Weather_Forecast_Data extends AsyncTask<Void, Void, JSONObject> {
    JSONObject data;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    public Date date;

    public static ArrayList<String> mDates = new ArrayList<>();
    public static ArrayList<String> mStatus = new ArrayList<>();
    public static ArrayList<String> mRange = new ArrayList<>();
    public static ArrayList<Integer> mIcon = new ArrayList<>();

    public Fetch_Weather_Forecast_Data(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        try{
            String latitude = MainActivity.latitude;
            String longitude = MainActivity.longitude;
            double lat;
            double lng;
            if(latitude != null && longitude != null) {
                lat = Double.parseDouble(latitude);
                lng = Double.parseDouble(longitude);
            } else {
                lat = 25.0136908;
                lng = 121.5406792;
            }
            URL url = new URL("http://api.openweathermap.org/data/2.5/forecast?lat="+ lat +"&lon="+ lng +"&units=metric&APPID=4b6acb19fa25f9433551446a312cad1a");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder json = new StringBuilder();
            String line = "";

            while ((line= reader.readLine())!=null) {
                json.append(line).append("\n");
            }
            reader.close();

            data = new JSONObject(json.toString());

            Integer count = 0;
            for(int i = 0; i < data.getJSONArray("list").length(); i++) {
                JSONObject list = data.getJSONArray("list").getJSONObject(i);
                String dates = list.getString("dt_txt");

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                date = sdf.parse(dates);

                if (System.currentTimeMillis() < date.getTime()) {
                    JSONObject main = list.getJSONObject("main");;
                    String tempMin = String.format("%.0f", main.getDouble("temp_min")) + "℃";
                    String tempMax = String.format("%.0f", main.getDouble("temp_max"))+"℃";

                    JSONObject weather = list.getJSONArray("weather").getJSONObject(0);
                    String weatherStatus = weather.getString("description");
                    int icon = weather.getInt("id");

                    String[] date = dates.split(" ");
                    date[1] = date[1].substring(0, date[1].length() - 3);

                    //icon
                    int id = icon / 100;
                    if(icon == 800){
                        mIcon.add(R.mipmap.ic_weather_sunny);
                    } else {
                        switch(id) {
                            case 2 : mIcon.add(R.mipmap.ic_weather_thunder);
                                break;
                            case 3 : mIcon.add(R.mipmap.ic_weather_drizzle);
                                break;
                            case 7 : mIcon.add(R.mipmap.ic_weather_foggy);
                                break;
                            case 8 : mIcon.add(R.mipmap.ic_weather_cloudy);
                                break;
                            case 6 : mIcon.add(R.mipmap.ic_weather_snowy);
                                break;
                            case 5 : mIcon.add(R.mipmap.ic_weather_rainy);
                                break;
                        }
                    }
                    mDates.add(date[1]);
                    mStatus.add(weatherStatus);
                    mRange.add(tempMin +" / "+tempMax);

                    count++;
                }
                if(count == 9) {
                    break;
                }
            }

            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        adapter = new CustomAdapterWeatherItem(mDates, mStatus, mRange, mIcon);
        recyclerView.setAdapter(adapter);
    }
}
