package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class Car_LocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap GMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car__location);

        //Google Maps
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapView_car);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        GMap = googleMap;

        GMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        LatLng currentLocation = Car_Parking_MenuActivity.latLng;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(currentLocation);
        markerOptions.title(Car_Parking_MenuActivity.title_position);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 18.0f));
    }

    public void toPrevious(View view)
    {
        Intent intent_CarParkingMenu = new Intent(this, Car_Parking_MenuActivity.class);
        startActivity(intent_CarParkingMenu);
        this.finish();
    }
}
