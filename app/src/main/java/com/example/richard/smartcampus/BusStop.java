package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

public class BusStop {

    @SerializedName("StopUID")
    public String stop_uid;

    public void setStop_UID(String stop_uid){
        this.stop_uid = stop_uid;
    }
}
