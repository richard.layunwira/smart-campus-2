package com.example.richard.smartcampus;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch_EMS_Appliances_Data extends AsyncTask< Void, Void, Void> {

    String data_fetch="";
    String data_1="";
    String data_2="";
    String data_3="";

    @Override
    protected Void doInBackground(Void... voids)
    {
        try{
            URL url_1 = new URL(EMS_MenuActivity.API_URL_1);
            HttpURLConnection connection_1 = (HttpURLConnection) url_1.openConnection();
            InputStream inputStream_1 = connection_1.getInputStream();
            BufferedReader bufferedReader_1 = new BufferedReader(new InputStreamReader(inputStream_1));
            String line_1 = "";

            while(line_1 != null)
            {
                data_fetch = data_fetch + line_1;
                line_1 = bufferedReader_1.readLine();
            }

            JSONObject root = new JSONObject(data_fetch);

            data_1 = root.getString("data");
            connection_1.disconnect();

            URL url_2 = new URL(EMS_MenuActivity.API_URL_2);
            HttpURLConnection connection_2 = (HttpURLConnection) url_2.openConnection();
            InputStream inputStream_2 = connection_2.getInputStream();
            BufferedReader bufferedReader_2 = new BufferedReader(new InputStreamReader(inputStream_2));
            String line_2 = "";
            data_fetch="";

            while(line_2 != null)
            {
                data_fetch = data_fetch + line_2;
                line_2 = bufferedReader_2.readLine();
            }

            root = new JSONObject(data_fetch);

            data_2 = root.getString("data");

            connection_2.disconnect();

            URL url_3 = new URL(EMS_MenuActivity.API_URL_3);
            HttpURLConnection connection_3 = (HttpURLConnection) url_3.openConnection();
            InputStream inputStream_3 = connection_3.getInputStream();
            BufferedReader bufferedReader_3 = new BufferedReader(new InputStreamReader(inputStream_3));
            String line_3 = "";
            data_fetch="";

            while(line_3 != null)
            {
                data_fetch = data_fetch + line_3;
                line_3 = bufferedReader_3.readLine();
            }

            root = new JSONObject(data_fetch);

            data_3 = root.getString("data");

            connection_3.disconnect();


        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        if (EMS_MenuActivity.menu_ems == 1 || EMS_MenuActivity.menu_ems == 2)
        {
            EMS_AppliancesActivity.text_val_1.setText(Float.toString(Float.parseFloat(data_1)/1000));
            EMS_AppliancesActivity.text_val_2.setText(Float.toString(Float.parseFloat(data_2)/1000));
            EMS_AppliancesActivity.text_val_3.setText(Float.toString(Float.parseFloat(data_3)/1000));
        }
        else
        {
            EMS_AppliancesActivity.text_val_1.setText(data_1);
            EMS_AppliancesActivity.text_val_2.setText(data_2);
            EMS_AppliancesActivity.text_val_3.setText(data_3);
        }

    }
}
