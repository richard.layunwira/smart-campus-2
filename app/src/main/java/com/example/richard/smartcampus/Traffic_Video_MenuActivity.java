package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Traffic_Video_MenuActivity extends AppCompatActivity {

    List<traffic_video_item> listData;

    public Thread t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traffic__video__menu);

        //List View
        listData = new ArrayList<>();
        listData.add(new traffic_video_item("IB Walkway Gate [1]", "140.118.17.105", "25.01314","121.53969"));
        listData.add(new traffic_video_item("IB Walkway Gate [2]", "140.118.17.104", "25.01314","121.53969"));
        listData.add(new traffic_video_item("IB 1st Floor [1]", "140.118.17.106", "25.01406","121.54095"));
        listData.add(new traffic_video_item("IB 1st Floor [2]", "140.118.17.107", "25.01406","121.54095"));
        listData.add(new traffic_video_item("IB Parking Gate", "140.118.17.103", "25.01279","121.54025"));

        listData.add(new traffic_video_item("NTUST Main gate [1]", "140.118.132.20", "25.01366","121.54041"));
        listData.add(new traffic_video_item("NTUST Main Gate [2]", "140.118.132.21", "25.01366","121.54041"));
        listData.add(new traffic_video_item("NTUST Main Gate [3]", "140.118.132.23", "25.01366","121.54041"));

        listData.add(new traffic_video_item("The First School Restaurant (Volleyball Court Gate) [1]", "140.118.34.50","25.0142","121.5425"));
        listData.add(new traffic_video_item("The First School Restaurant (Volleyball Court Gate) [2]", "140.118.34.54","25.0142","121.5425"));
        listData.add(new traffic_video_item("The First School Restaurant (Post Office Gate) [1]", "140.118.34.51", "25.0154","121.5421"));
        listData.add(new traffic_video_item("The First School Restaurant (Post Office Gate) [2]", "140.118.34.52", "25.0154","121.5421"));
        listData.add(new traffic_video_item("The First School Restaurant (ATM Gate)", "140.118.34.53", "25.0141","121.5425"));
//        listData.add(new traffic_video_item("Third Canteen Outside 6", "140.118.34.55","25.0156093","121.5427229"));

        listData.add(new traffic_video_item("Running Court [1]", "140.118.45.29","25.0151","121.5426"));
        listData.add(new traffic_video_item("Running Court [2]", "140.118.45.30","25.0151","121.5426"));

        listData.add(new traffic_video_item("TR Parking Gate", "140.118.58.21","25.0148","121.5424"));
        listData.add(new traffic_video_item("TR Parking (Motorcycle Ticket Machine)", "140.118.58.24","25.0148","121.5424"));
        listData.add(new traffic_video_item("TR Parking (Motorcycle Driveway)", "140.118.58.27","25.0148","121.5424"));
        listData.add(new traffic_video_item("TR Parking Lot (Motorcycle) [1]", "140.118.58.22","25.0148","121.5424"));
        listData.add(new traffic_video_item("TR Parking Lot (Motorcycle) [2]", "140.118.58.23","25.0148","121.5424"));

        listData.add(new traffic_video_item("TR Parking (Car Driveway)", "140.118.58.25","25.0171","121.5414"));
        listData.add(new traffic_video_item("TR Parking (Car Ticket Machine)", "140.118.58.26","25.0171","121.5414"));


        ListView listView = findViewById(R.id.listTrafficVideo);

        CustomAdapterTrafficVideoItem adapter = new CustomAdapterTrafficVideoItem(this, R.layout.list_item_traffic_video, listData);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("VideoName", listData.get(position).videoName);
                intent.putExtra("VideoAddress", listData.get(position).videoAddress);
                intent.putExtra("VideoLatitude", listData.get(position).videoLatitude);
                intent.putExtra("VideoLongitude", listData.get(position).videoLongitude);

                intent.setClass(Traffic_Video_MenuActivity.this, Traffic_Video_Activity.class);
                startActivity(intent);
            }
        });

        //Datetime
        t = new Thread() {
            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(10);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tDate = findViewById(R.id.textViewDate);
                                TextView tHour = findViewById(R.id.textViewHour);
                                long date = System.currentTimeMillis();
                                SimpleDateFormat sdfDate = new SimpleDateFormat("MMM, dd yyyy");
                                SimpleDateFormat sdfHour = new SimpleDateFormat("hh:mm:ss");
                                String dateString = sdfDate.format(date);
                                String hourString = sdfHour.format(date);
                                tDate.setText(dateString);
                                tHour.setText(hourString);
                            }
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        t.start();
    }

    public void toPrevious(View view)
    {
        t.interrupt();

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

        this.finish();

    }
}
