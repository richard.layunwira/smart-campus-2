package com.example.richard.smartcampus;

import com.google.gson.annotations.SerializedName;

public class RouteName {

    @SerializedName("Zh_tw")
    public String Zh_tw;

    @SerializedName("En")
    public String En;

    public void setStationTW(String Zh_tw) {
        this.Zh_tw = Zh_tw;
    }

    public void setStationEn(String En) {
        this.En = En;
    }

}
