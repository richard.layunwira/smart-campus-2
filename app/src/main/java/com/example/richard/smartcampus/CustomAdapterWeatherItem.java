package com.example.richard.smartcampus;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapterWeatherItem extends RecyclerView.Adapter<CustomAdapterWeatherItem.ViewHolder> {

    private ArrayList<String> mDates;
    private ArrayList<String> mRange;
    private ArrayList<String> mStatus;
    private ArrayList<Integer> mIcon;

    public CustomAdapterWeatherItem(ArrayList<String> mDates, ArrayList<String> mStatus, ArrayList<String> mRange, ArrayList<Integer> mIcon) {
        this.mDates = mDates;
        this.mRange = mRange;
        this.mStatus = mStatus;
        this.mIcon = mIcon;
    }

    @NonNull
    @Override
    public CustomAdapterWeatherItem.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_weather, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull CustomAdapterWeatherItem.ViewHolder holder, int position) {
        holder.mDate.setText(mDates.get(position));
        holder.mRange.setText(mRange.get(position));
        holder.mStatus.setText(mStatus.get(position));
        holder.mIcon.setImageResource(mIcon.get(position));
    }

    @Override
    public int getItemCount() {
        if(mDates == null) {
            return 0;
        } else {
            return mDates.size();
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mDate;
        public TextView mRange;
        public TextView mStatus;
        public ImageView mIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            mDate = itemView.findViewById(R.id.textViewDate);
            mRange = itemView.findViewById(R.id.textViewTempRange);
            mStatus = itemView.findViewById(R.id.textViewStatus);
            mIcon = itemView.findViewById(R.id.imageViewWeather);
        }
    }
}
