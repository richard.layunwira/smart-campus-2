package com.example.richard.smartcampus;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch_EMSMenu_Data extends AsyncTask< Void, Void, Void> {

    String data_fetch="";
    String usage_data="";
    String max_demand_data="";
    String pf_data="";
    String cost_data="";

    @Override
    protected Void doInBackground(Void... voids)
    {
        try{
            URL url = new URL("http://140.118.101.215:3000/ems/total/33000509b52f1001");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";

            while(line != null)
            {
                data_fetch = data_fetch + line;
                line = bufferedReader.readLine();
            }

            JSONObject root = new JSONObject(data_fetch);

            usage_data = Float.toString(Math.round(Float.parseFloat(root.getString("usage"))/1000));
            max_demand_data = Integer.toString(Integer.parseInt(root.getString("demand"))/1000);
            pf_data = root.getString("power");
            cost_data = Float.toString(Float.parseFloat(usage_data)*3000);

            connection.disconnect();

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        EMS_MenuActivity.value1.setText(this.usage_data);
        EMS_MenuActivity.value2.setText(this.max_demand_data);
        EMS_MenuActivity.value3.setText(this.pf_data);
        EMS_MenuActivity.value4.setText(this.cost_data);
    }

}
