package com.example.richard.smartcampus;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterBusScheduleItem extends ArrayAdapter<bus_schedule_item> {

    Context context;
    int layoutResourceId;
    List<bus_schedule_item> data = null;

    public CustomAdapterBusScheduleItem(@NonNull Context context, int resource, @NonNull List<bus_schedule_item> objects) {
        super(context, resource, objects);

        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    static class BusScheduleItem {
        TextView bus_header_1;
        TextView bus_plate_name;
        TextView bus_position;
        TextView bus_time;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CustomAdapterBusScheduleItem.BusScheduleItem BusScheduleItem = null;

        if(convertView == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();

            convertView = inflater.inflate(layoutResourceId, parent, false);

            BusScheduleItem = new CustomAdapterBusScheduleItem.BusScheduleItem();
            BusScheduleItem.bus_header_1 = (TextView)convertView.findViewById(R.id.text_header_1);
            BusScheduleItem.bus_plate_name = (TextView)convertView.findViewById(R.id.text_value_1);
            BusScheduleItem.bus_position = (TextView)convertView.findViewById(R.id.text_value_2);
            BusScheduleItem.bus_time = (TextView)convertView.findViewById(R.id.text_value_3);

            convertView.setTag(BusScheduleItem);
        }
        else {
            BusScheduleItem = (CustomAdapterBusScheduleItem.BusScheduleItem)convertView.getTag();
        }

        bus_schedule_item dataItem = data.get(position);
        BusScheduleItem.bus_header_1.setText(dataItem.header_1);
        BusScheduleItem.bus_plate_name.setText(dataItem.plate_number);
        BusScheduleItem.bus_position.setText(dataItem.position_bus);
        BusScheduleItem.bus_time.setText(dataItem.time_bus);

        return convertView;
    }

}
