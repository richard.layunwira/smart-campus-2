package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;

public class EMS_MenuActivity extends AppCompatActivity {

    public static TextView value1;
    public static TextView value2;
    public static TextView value3;
    public static TextView value4;

    public static String API_URL_1="";
    public static String API_URL_2="";
    public static String API_URL_3="";
    public static int menu_ems = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ems__menu);

        value1 = (TextView) findViewById(R.id.text_val_1);
        value2 = (TextView) findViewById(R.id.text_val_2);
        value3 = (TextView) findViewById(R.id.text_val_3);
        value4 = (TextView) findViewById(R.id.text_val_4);

        Fetch_EMSMenu_Data process_FetchData = new Fetch_EMSMenu_Data();
        process_FetchData.execute();

        Thread t_TimeDate = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    while (!isInterrupted())
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView time_date = (TextView) findViewById(R.id.time_date);
                                long date = System.currentTimeMillis();
                                String dataString = DateFormat.getDateInstance(0).format(date) + '\n' + DateFormat.getTimeInstance(2).format(date);
                                time_date.setText(dataString);
                            }
                        });
                        Thread.sleep(10);
                    }
                } catch (InterruptedException e) {}

            }
        };

        t_TimeDate.start();

    }

    public void toPrevious(View view)
    {
        Intent intent_MainActivity = new Intent(this, MainActivity.class);
        startActivity(intent_MainActivity);
    }

    public void Go_to_EMS_Appliances_1(View view)
    {
        menu_ems = 1;
        API_URL_1 = "http://140.118.101.215:3000/ems/energy-usage/33000509b52f1006";
        API_URL_2 = "http://140.118.101.215:3000/ems/energy-usage/33000509b52f1007";
        API_URL_3 = "http://140.118.101.215:3000/ems/energy-usage/33000509b52f1008";
        Intent intent = new Intent(this, EMS_AppliancesActivity.class);
        startActivity(intent);
    }

    public void Go_to_EMS_Appliances_2(View view)
    {
        menu_ems = 2;
        API_URL_1 = "http://140.118.101.215:3000/ems/demand/33000509b52f1006";
        API_URL_2 = "http://140.118.101.215:3000/ems/demand/33000509b52f1007";
        API_URL_3 = "http://140.118.101.215:3000/ems/demand/33000509b52f1008";
        Intent intent = new Intent(this, EMS_AppliancesActivity.class);
        startActivity(intent);
    }

    public void Go_to_EMS_Appliances_3(View view)
    {
        menu_ems = 3;
        API_URL_1 = "http://140.118.101.215:3000/ems/power-factor/33000509b52f1006";
        API_URL_2 = "http://140.118.101.215:3000/ems/power-factor/33000509b52f1007";
        API_URL_3 = "http://140.118.101.215:3000/ems/power-factor/33000509b52f1008";
        Intent intent = new Intent(this, EMS_AppliancesActivity.class);
        startActivity(intent);
    }

    public void View_Chart_Demand(View view) {
        Intent intent_chart_demand = new Intent(EMS_MenuActivity.this, View_Demand.class);
        startActivity(intent_chart_demand);
    }

    public void View_Chart_Usage(View view) {
        Intent intent_chart_usage = new Intent(EMS_MenuActivity.this, View_Usage.class);
        startActivity(intent_chart_usage);
    }

}
