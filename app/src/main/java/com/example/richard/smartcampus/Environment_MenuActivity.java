package com.example.richard.smartcampus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

public class Environment_MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment__menu);

        String url = "http://140.118.101.189:4000/airbox/phone/map";
        WebView view = (WebView) findViewById(R.id.enviro_quality_view);
        view.getSettings().setJavaScriptEnabled(true);
        view.loadUrl(url);
    }

    public void toPrevious(View view)
    {
        Intent intent_MainActivity = new Intent(this, MainActivity.class);
        startActivity(intent_MainActivity);
    }
}
