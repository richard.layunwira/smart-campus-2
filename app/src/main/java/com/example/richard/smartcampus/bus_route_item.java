package com.example.richard.smartcampus;

public class bus_route_item {

    String bus_route_name;
    String bus_destination;
    String bus_route_id;

    public bus_route_item(String bus_route_name, String bus_destination, String bus_route_id)
    {
        this.bus_route_name = bus_route_name;
        this.bus_destination = bus_destination;
        this.bus_route_id = bus_route_id;
    }

}
