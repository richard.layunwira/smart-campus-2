package com.example.richard.smartcampus;

public class traffic_video_item {

    String videoName, videoAddress, videoLatitude, videoLongitude;

    public traffic_video_item(String videoName, String videoAddress, String videoLatitude, String videoLongitude) {
        this.videoName = videoName;
        this.videoAddress = videoAddress;
        this.videoLatitude = videoLatitude;
        this.videoLongitude = videoLongitude;
    }

}
