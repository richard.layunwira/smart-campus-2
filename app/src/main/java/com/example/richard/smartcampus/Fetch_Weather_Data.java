package com.example.richard.smartcampus;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;

public class Fetch_Weather_Data extends AsyncTask<Void, Void, Void> {

    String lastUpdated;
    String weatherStatus;
    String weatherStatusMain;
    String temp;
    String tempMin;
    String tempMax;
    long sunrise;
    long sunset;
    String location;
    int icon;
    String latitude, longitude;
    double lat, lng;

    @Override
    protected Void doInBackground(Void... voids) {
        try{
            latitude = MainActivity.latitude;
            longitude = MainActivity.longitude;
            if(latitude != null && longitude != null) {
                lat = Double.parseDouble(latitude);
                lng = Double.parseDouble(longitude);
            } else {
                lat = 25.0136908;
                lng = 121.5406792;
            }
            URL url = new URL("http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lng+"&units=metric&APPID=4b6acb19fa25f9433551446a312cad1a");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder json = new StringBuilder();
            String line = "";

            while ((line= reader.readLine())!=null) {
                json.append(line).append("\n");
            }
            reader.close();

            JSONObject data = new JSONObject(json.toString());
            if(data.getInt("cod") != 200) {
                return null;
            } else {
                JSONObject weather = data.getJSONArray("weather").getJSONObject(0);
                weatherStatus = weather.getString("description");
                weatherStatusMain = weather.getString("main");
                icon = weather.getInt("id");

                JSONObject main = data.getJSONObject("main");
                temp = String.format("%.0f", main.getDouble("temp"))+"℃";
                tempMin = String.format("%.0f", main.getDouble("temp_min"))+"℃";
                tempMax = String.format("%.0f", main.getDouble("temp_max"))+"℃";

                JSONObject sys = data.getJSONObject("sys");
                sunrise = sys.getLong("sunrise");
                sunset = sys.getLong("sunset");;

                location = data.getString("name");

                DateFormat df = DateFormat.getDateTimeInstance();
                lastUpdated = df.format(new Date(data.getLong("dt")*1000));
            }

            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void v)
    {
        super.onPostExecute(v);
        MainActivity.txtTemp.setText(this.temp);
        MainActivity.txtStatus.setText(this.weatherStatus);
        MainActivity.txtTempRange.setText(this.tempMin+" / "+this.tempMax);
        MainActivity.txtLocation.setText(this.location);
        MainActivity.txtLastUpdated.setText("Updated on: "+this.lastUpdated);

        //icon
        int id = icon / 100;
        if(icon == 800){
            long currentTime = new Date().getTime();
            if(currentTime >= (sunrise*1000) && currentTime < (sunset*1000)) {
                MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_sunny);
            } else {
                MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_clear_night);
            }
        } else {
            switch(id) {
                case 2 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_thunder);
                    break;
                case 3 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_drizzle);
                    break;
                case 7 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_foggy);
                    break;
                case 8 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_cloudy);
                    break;
                case 6 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_snowy);
                    break;
                case 5 : MainActivity.imgWeather.setImageResource(R.mipmap.ic_weather_rainy);
                    break;
            }
        }
    }
}
