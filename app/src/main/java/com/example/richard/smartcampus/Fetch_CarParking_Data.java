package com.example.richard.smartcampus;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class Fetch_CarParking_Data extends AsyncTask< Void, Void, Void> {

    String data_ParkingTR = "";
    String data_ParkingIB = "";
    String no_car_ParkingTR = "";
    String no_car_ParkingIB = "";

    String data_PredictionTR = "";

    public int[] temp_pred;
    public String[] temp_time_pred;

    @Override
    protected Void doInBackground(Void... voids)
    {
        try{

            URL url_ParkingTR = new URL("http://140.118.101.189:4000/parking-elastic/data");
            HttpURLConnection httpURLConnection_ParkingTR = (HttpURLConnection) url_ParkingTR.openConnection();
            InputStream inputStream_ParkingTR = httpURLConnection_ParkingTR.getInputStream();
            BufferedReader bufferedReader_ParkingTR = new BufferedReader(new InputStreamReader(inputStream_ParkingTR));
            String line_ParkingTR = "";

            while(line_ParkingTR != null)
            {
                data_ParkingTR = data_ParkingTR + line_ParkingTR;
                line_ParkingTR = bufferedReader_ParkingTR.readLine();
            }

            JSONObject root_ParkingTR = new JSONObject(data_ParkingTR);
            JSONObject hits_ParkingTR = root_ParkingTR.getJSONObject("hits");
            JSONArray hits_array_ParkingTR = hits_ParkingTR.getJSONArray("hits");
            JSONObject temp_ParkingTR = hits_array_ParkingTR.getJSONObject(0);
            JSONObject source_ParkingTR = temp_ParkingTR.getJSONObject("_source");
            no_car_ParkingTR = source_ParkingTR.getString("carinpark");

            // =================================================================================================================

            URL url_ParkingIB = new URL("http://140.118.101.189:4000/parking-elastic/data-ib");
            HttpURLConnection httpURLConnection_ParkingIB = (HttpURLConnection) url_ParkingIB.openConnection();
            InputStream inputStream_ParkingIB = httpURLConnection_ParkingIB.getInputStream();
            BufferedReader bufferedReader_ParkingIB = new BufferedReader(new InputStreamReader(inputStream_ParkingIB));
            String line_ParkingIB = "";

            while(line_ParkingIB != null)
            {
                data_ParkingIB = data_ParkingIB + line_ParkingIB;
                line_ParkingIB = bufferedReader_ParkingIB.readLine();
            }

            JSONObject root_ParkingIB = new JSONObject(data_ParkingIB);
            JSONObject hits_ParkingIB = root_ParkingIB.getJSONObject("hits");
            JSONArray hits_array_ParkingIB = hits_ParkingIB.getJSONArray("hits");
            JSONObject temp_ParkingIB = hits_array_ParkingIB.getJSONObject(0);
            JSONObject source_ParkingIB = temp_ParkingIB.getJSONObject("_source");
            no_car_ParkingIB = source_ParkingIB.getString("carinpark");

            httpURLConnection_ParkingTR.disconnect();
            httpURLConnection_ParkingIB.disconnect();

            // =================================================================================================================
            URL url_PredictionTR = new URL("http://140.118.101.189:4000/parking/forecast");
            HttpURLConnection httpURLConnection_PredictionTR = (HttpURLConnection) url_PredictionTR.openConnection();
            InputStream inputStream_PredictionTR = httpURLConnection_PredictionTR.getInputStream();
            BufferedReader bufferedReader_PredictionTR = new BufferedReader(new InputStreamReader(inputStream_PredictionTR));
            String line_PredictionTR = "";

            while(line_PredictionTR != null)
            {
                data_PredictionTR = data_PredictionTR + line_PredictionTR;
                line_PredictionTR = bufferedReader_PredictionTR.readLine();
            }

            if (data_PredictionTR != null) {
                Log.d("PredictionTR", data_PredictionTR);
            }
            else {
                Log.d("PredictionTR", "asdasdsadsadsadsad");
            }

            temp_pred = new int[3];
            temp_time_pred = new String[3];
            JSONObject root_PredictionTR = new JSONObject(data_PredictionTR);
            JSONArray array_PredictionTR = root_PredictionTR.getJSONArray("data");

            for(int i=0;i<3;i++)
            {
                JSONObject temp_PredictionTR = array_PredictionTR.getJSONObject(i);
                JSONObject temp2_PredictionTR = temp_PredictionTR.getJSONObject("_source");

                temp_pred[i] = temp2_PredictionTR.getInt("carinparkForecast");
                temp_time_pred[i] = temp2_PredictionTR.getString("UploadTimeForecast");
            }


            httpURLConnection_PredictionTR.disconnect();

        } catch (MalformedURLException e)
        {
            e.printStackTrace();
        } catch (IOException e)
        {
            e.printStackTrace();
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid)
    {
        super.onPostExecute(aVoid);
        Car_Parking_MenuActivity.no_car_ParkingTR.setText(Integer.toString(300-Integer.parseInt(this.no_car_ParkingTR)));
        Car_Parking_MenuActivity.ProgressBar_CarTR.setProgress(Integer.parseInt(this.no_car_ParkingTR));
        Car_Parking_MenuActivity.no_car_ParkingIB.setText(Integer.toString(1200-Integer.parseInt(this.no_car_ParkingIB)));
        Car_Parking_MenuActivity.ProgressBar_CarIB.setProgress(Integer.parseInt(this.no_car_ParkingIB));

        Car_Parking_MenuActivity.time_pred_1.setText(temp_time_pred[0].substring(11,16));
        Car_Parking_MenuActivity.time_pred_2.setText(temp_time_pred[1].substring(11,16));
        Car_Parking_MenuActivity.time_pred_3.setText(temp_time_pred[2].substring(11,16));

        Car_Parking_MenuActivity.val_pred_1.setText(Integer.toString(300-temp_pred[0]));
        Car_Parking_MenuActivity.val_pred_2.setText(Integer.toString(300-temp_pred[1]));
        Car_Parking_MenuActivity.val_pred_3.setText(Integer.toString(300-temp_pred[2]));

    }
}
