package com.example.richard.smartcampus;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;

public class EMS_AppliancesActivity extends AppCompatActivity {

    private TextView text_header;

    private TextView text_context_1;
    private TextView text_context_2;
    private TextView text_context_3;

    private ImageView image_1;
    private ImageView image_2;
    private ImageView image_3;

    public static TextView text_val_1;
    public static TextView text_val_2;
    public static TextView text_val_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ems__appliances);

        Log.d("asdasdasd",Integer.toString(EMS_MenuActivity.menu_ems));

        image_1 = (ImageView) findViewById(R.id.menu_1);
        image_2 = (ImageView) findViewById(R.id.menu_2);
        image_3 = (ImageView) findViewById(R.id.menu_3);

        text_context_1 = (TextView) findViewById(R.id.text_context_1);
        text_context_2 = (TextView) findViewById(R.id.text_context_2);
        text_context_3 = (TextView) findViewById(R.id.text_context_3);

        text_val_1 = (TextView) findViewById(R.id.text_val_1);
        text_val_2 = (TextView) findViewById(R.id.text_val_2);
        text_val_3 = (TextView) findViewById(R.id.text_val_3);

        text_header = (TextView) findViewById(R.id.header);

        Fetch_EMS_Appliances_Data process_FetchData = new Fetch_EMS_Appliances_Data();
        process_FetchData.execute();

        if(EMS_MenuActivity.menu_ems == 1)
        {
            text_header.setText("TR Building Usage");
            text_context_1.setText("Air Conditioner (MWh)");
            text_context_2.setText("Plugs (MWh)");
            text_context_3.setText("Lighting (MWh)");

            image_1.setBackgroundColor(Color.rgb(66, 134, 244));
            image_2.setBackgroundColor(Color.rgb(66, 134, 244));
            image_3.setBackgroundColor(Color.rgb(66, 134, 244));
        }
        else if(EMS_MenuActivity.menu_ems == 2)
        {
            text_header.setText("TR Building Demand");
            text_context_1.setText("Air Conditioner (kW)");
            text_context_2.setText("Plugs (kW)");
            text_context_3.setText("Lighting (kW)");
            image_1.setBackgroundColor(Color.rgb(240, 173, 78));
            image_2.setBackgroundColor(Color.rgb(240, 173, 78));
            image_3.setBackgroundColor(Color.rgb(240, 173, 78));
        }
        else if(EMS_MenuActivity.menu_ems == 3)
        {
            text_header.setText("TR Building PF");
            text_context_1.setText("Air Conditioner (%)");
            text_context_2.setText("Plugs (%)");
            text_context_3.setText("Lighting (%)");
            image_1.setBackgroundColor(Color.rgb(197, 90, 17));
            image_2.setBackgroundColor(Color.rgb(197, 90, 17));
            image_3.setBackgroundColor(Color.rgb(197, 90, 17));
        }

        Thread t_TimeDate = new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    while (!isInterrupted())
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView time_date = (TextView) findViewById(R.id.time_date);
                                long date = System.currentTimeMillis();
                                String dataString = DateFormat.getDateInstance(0).format(date) + '\n' + DateFormat.getTimeInstance(2).format(date);
                                time_date.setText(dataString);
                            }
                        });
                        Thread.sleep(10);
                    }
                } catch (InterruptedException e) {}

            }
        };

        t_TimeDate.start();

    }

    public void toPrevious(View view)
    {
        Intent intent_EMS_Menu = new Intent(this, EMS_MenuActivity.class);
        startActivity(intent_EMS_Menu);
    }
}
